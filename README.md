# Substrate index comparison

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Compare the substrate index layers produced by 2 methods of generating categorical substrate model. 
Original uses randomForest package in R and new uses the ranger package.


## Summary
Reason for changing packages was that ranger supports case-weighting to balance the classes in the input training data. 
Essentially, this was done to reduce the prevalence of rock in the output and produce a more balanced predicted surface. 
Note, that in some regions (such as the Shelf), there is actually a greater prevalence of rock in the categorical substrate and 
the rocky index layer. 


## Status
Ongoing-improvements


## Contents
Jupyter notebook with functions and code executed to display plots of difference rasters. 


## Methods
Rasterio used to load datasets (GeoTIFFs) and then read the band as a numpy array, masking out nodata values. The random forest array is then 
subtracted from the ranger array (ranger_substrate - rf_substrate). The array is then saved as a GeoTIFF using the profile from the input rasters with a float32 data type. 
Plots and histograms of the difference are generated and saved to disk as well. 


## Requirements
Users should be able to use the yml file in the repository to create a virtual python env to execute the code. 
`conda env create -f <environment-name>.yml`


## Caveats
Input rasters must already be normalized on a 0-1 scale. Positive values in plots and GeoTIFFs represent areas where the ranger model 
has a greater density of the index layer being examined (rocky, mixed, sandy, muddy). These are represented in red on the plots. 
Negative values (represented in blue on the plots) indicate areas where the ranger model has a lower density of the index layer being examined.


## Uncertainty
Output plots appear to not show entire extent for some reason. Uncertain if it's the colour scheme or masking. 
However, the output GeoTIFF files appear correct.


## Acknowledgements
Jessica Nephin


## References
https://rasterio.readthedocs.io/en/latest/quickstart.html
https://rasterio.readthedocs.io/en/latest/topics/plotting.html
https://www.earthdatascience.org/workshops/gis-open-source-python/
